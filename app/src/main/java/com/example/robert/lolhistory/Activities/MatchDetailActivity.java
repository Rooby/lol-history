package com.example.robert.lolhistory.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.robert.lolhistory.MMApplication;
import com.example.robert.lolhistory.Adapters.MatchDetailsAdapter;
import com.example.robert.lolhistory.Models.APIKeys;
import com.example.robert.lolhistory.Models.GameModel;
import com.example.robert.lolhistory.Models.Participant;
import com.example.robert.lolhistory.Models.ParticipantIdentity;
import com.example.robert.lolhistory.Models.Team;
import com.example.robert.lolhistory.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MatchDetailActivity extends AppCompatActivity {
    private String apiKey = APIKeys.getApiKey();
    public RecyclerView mRecyclerView;
    private List<ParticipantIdentity> participantIdentitiesList;
    private List<Participant> participantList;
    private List<Team> teamList;
    private long gameId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_detail);

        gameId = getIntent().getLongExtra("gameId", 0);
        mRecyclerView = (RecyclerView) findViewById(R.id.match_details);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MatchDetailActivity.this));
        getMatchDetails();
    }

    private void getMatchDetails() {
        String url = "https://eun1.api.riotgames.com";

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, url + "/lol/match/v3/matches/" + gameId + "?api_key=" + apiKey,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Volley", "Response is: " + response);
                        Gson gson = new Gson();
                        Type listType = new TypeToken<GameModel>() {}.getType();
                        GameModel game = gson.fromJson(response, listType);
                        Log.d("Volley", "Game is: " + game);

                        participantIdentitiesList = new ArrayList<ParticipantIdentity>();
                        participantList = new ArrayList<Participant>();
                        teamList = new ArrayList<Team>();

                        for (ParticipantIdentity participantIdentity: game.getParticipantIdentities()) {
                            participantIdentitiesList.add(participantIdentity);
                        }
                        for (Participant participant: game.getParticipants()) {
                            participantList.add(participant);
                        }
                        for (Team team: game.getTeams()) {
                            teamList.add(team);
                        }

                        mRecyclerView.setAdapter(new MatchDetailsAdapter(participantIdentitiesList, participantList, teamList, MatchDetailActivity.this));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Volley", error.getMessage());
                    }
                });
        RequestQueue queue = MMApplication.getQue();
        queue.add(stringRequest);
    }
}
