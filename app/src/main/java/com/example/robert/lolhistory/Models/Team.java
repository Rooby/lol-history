package com.example.robert.lolhistory.Models;

public class Team {
    private boolean firstDragon;
    private boolean firstInhibitor;
    private String win;
    private boolean firstRiftHerald;
    private boolean firstBaron;
    private int baronKills;
    private int riftHeraldKills;
    private boolean firstBlood;

    public boolean isFirstDragon() {
        return firstDragon;
    }

    public boolean isFirstInhibitor() {
        return firstInhibitor;
    }

    public String getWin() {
        return win;
    }

    public boolean isFirstRiftHerald() {
        return firstRiftHerald;
    }

    public boolean isFirstBaron() {
        return firstBaron;
    }

    public int getBaronKills() {
        return baronKills;
    }

    public int getRiftHeraldKills() {
        return riftHeraldKills;
    }

    public boolean isFirstBlood() {
        return firstBlood;
    }

    public boolean isFirstTower() {
        return firstTower;
    }

    public int getInhibitorKills() {
        return inhibitorKills;
    }

    public int getTowerKills() {
        return towerKills;
    }

    public int getDragonKills() {
        return dragonKills;
    }

    private boolean firstTower;
    private int inhibitorKills;
    private int towerKills;
    private int dragonKills;
}
