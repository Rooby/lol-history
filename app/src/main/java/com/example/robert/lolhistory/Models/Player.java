package com.example.robert.lolhistory.Models;

public class Player {
    private String summonerName;
    private int profileIcon;
    private long summonerId;
    private long accountId;

    public String getSummonerName() {
        return summonerName;
    }

    public int getProfileIcon() {
        return profileIcon;
    }

    public long getSummonerId() {
        return summonerId;
    }

    public long getAccountId() {
        return accountId;
    }
}
