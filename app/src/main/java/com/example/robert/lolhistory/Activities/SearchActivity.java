package com.example.robert.lolhistory.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.robert.lolhistory.MMApplication;
import com.example.robert.lolhistory.Models.APIKeys;
import com.example.robert.lolhistory.Models.SummonerModel;
import com.example.robert.lolhistory.R;
import com.example.robert.lolhistory.Helpers.Tools;
import com.google.gson.Gson;

import java.util.Locale;

public class SearchActivity extends AppCompatActivity {

    private TextView summonerNameTextView;
    private DrawerLayout drawerLayout;
    private FrameLayout frameLayout;
    private ListView drawerList;

    private String apiKey = APIKeys.getApiKey();

    private ActionBarDrawerToggle drawerToggle;
    private String[] drawerListText;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private Locale locale;
    private static final String LOCALE_KEY = "localekey";
    private static final String ROMANIAN_LOCALE = "ro";
    private static final String ENGLISH_LOCALE = "en";

    private String summonerSearched;


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setupDrawer();
        setupSearchBtn();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocalePreferences();
    }

    private void setupDrawer() {
        mTitle = mDrawerTitle = getTitle();
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle(mTitle);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        frameLayout = (FrameLayout) findViewById(R.id.content_frame);
        drawerList = (ListView) findViewById(R.id.drawer_list);

        drawerListText = getResources().getStringArray(R.array.drawer_labels);
        View headerView = getLayoutInflater().inflate(R.layout.drawer_header, null);

        drawerList.addHeaderView(headerView, null, false);
        drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, drawerListText));

        setupDrawerListeners();
        setupDrawerToggleEffect(myToolbar);
    }

    private void setupDrawerListeners() {
        drawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 2:
                        updateLocalePreferences(ENGLISH_LOCALE, LOCALE_KEY);
                        break;

                    case 3:
                        updateLocalePreferences(ROMANIAN_LOCALE, LOCALE_KEY);
                        break;
                }
                recreate();
                drawerLayout.closeDrawer(drawerList);
            }
        });
    }

    private void setupDrawerToggleEffect(Toolbar myToolbar) {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, myToolbar, R.string.open_drawer, R.string.close_drawer) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void setupSearchBtn() {
        Button searchBtn = findViewById(R.id.searchBtn);
        summonerNameTextView = findViewById(R.id.summonerName);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Tools.isNetworkAvailable(getApplicationContext())) {
                    summonerSearched = getResources().getString(R.string.summoner_searched);
                    SharedPreferences state = getSharedPreferences(summonerSearched, MODE_PRIVATE);
                    Gson gson = new Gson();
                    SummonerModel summoner = gson.fromJson(state.getString("summonerName", ""), SummonerModel.class);

                    Intent intent = new Intent(getApplicationContext(), MatchHistoryActivity.class);
                    intent.putExtra("SummonerModel", summoner);
                    startActivity(intent);
                } else {
                    String name = summonerNameTextView.getText().toString();
                    String url = "https://eun1.api.riotgames.com";
                    StringRequest stringRequest = new StringRequest(
                            Request.Method.GET, url + "/lol/summoner/v3/summoners/by-name/" + name + "?api_key=" + apiKey,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("Volley", "Response is: " + response);
                                    Gson gson = new Gson();
                                    SummonerModel summoner = gson.fromJson(response, SummonerModel.class);

                                    summonerSearched = getResources().getString(R.string.summoner_searched);
                                    SharedPreferences state = getSharedPreferences(summonerSearched, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = state.edit();
                                    editor.putString("summonerName", gson.toJson(summoner));
                                    editor.commit();

                                    Intent intent = new Intent(getApplicationContext(), MatchHistoryActivity.class);
                                    intent.putExtra("SummonerModel", summoner);
                                    startActivity(intent);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Volley", error.getMessage());
                                }
                            });
                    RequestQueue queue = MMApplication.getQue();
                    queue.add(stringRequest);
                }
            }
        });
    }

    private void getLocalePreferences() {
        SharedPreferences state = getSharedPreferences(LOCALE_KEY, MODE_PRIVATE);
        String locale = state.getString(LOCALE_KEY, "en");
        updateLocalePreferences(locale, LOCALE_KEY);
    }

    private void updateLocalePreferences(String localeKey, String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(key, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Locale locale = new Locale(localeKey);
        editor.putString(key, localeKey);

        //editor.apply();
        editor.commit();
        Configuration configuration = getResources().getConfiguration();
        configuration.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(configuration,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
