package com.example.robert.lolhistory.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.robert.lolhistory.Helpers.DownloadImageTask;
import com.example.robert.lolhistory.Helpers.Tools;
import com.example.robert.lolhistory.MMApplication;
import com.example.robert.lolhistory.Adapters.MatchHistoryAdapter;
import com.example.robert.lolhistory.Models.APIKeys;
import com.example.robert.lolhistory.Models.ChampionMastery;
import com.example.robert.lolhistory.Models.ChampionMasteryList;
import com.example.robert.lolhistory.Models.Match;
import com.example.robert.lolhistory.Models.MatchListModel;
import com.example.robert.lolhistory.Models.SummonerModel;
import com.example.robert.lolhistory.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MatchHistoryActivity extends AppCompatActivity {
    private String apiKey = APIKeys.getApiKey();
    private SummonerModel summoner;
    public RecyclerView mRecyclerView;
    private List<Object> mListItems = new ArrayList<Object>();;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 12;
    private MatchHistoryAdapter matchHistoryAdapter = new MatchHistoryAdapter(mListItems, MatchHistoryActivity.this);
    private String summonerSearched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_history);

        String imagePath = "http://ddragon.leagueoflegends.com/cdn/8.11.1/img/profileicon/";
        summoner = (SummonerModel) getIntent().getSerializableExtra("SummonerModel");

        TextView summonerName = findViewById(R.id.summoner_name);
        summonerName.setText(summoner.getName());

        ImageView summonerIcon = findViewById(R.id.summoner_icon);
        new DownloadImageTask(summonerIcon).execute(imagePath + summoner.getProfileIconId() + ".png");

        mLayoutManager = new LinearLayoutManager(MatchHistoryActivity.this);
        mRecyclerView = (RecyclerView) findViewById(R.id.match_history_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (!Tools.isNetworkAvailable(getApplicationContext())) {
            summonerSearched = getResources().getString(R.string.summoner_searched);
            SharedPreferences state = getSharedPreferences(summonerSearched, MODE_PRIVATE);

            Gson gson = new Gson();
            Type listType = new TypeToken<List<Match>>() {}.getType();
            String strObj = state.getString("summonerMatchHistory", "");
            List<Match> summonerMatchHistory = gson.fromJson(strObj, listType);
            for (Match match: summonerMatchHistory
                 ) {
                mListItems.add(match);
            }
            mRecyclerView.setAdapter(matchHistoryAdapter);
        } else {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {
                    if(dy > 0) //check for scroll down
                    {
                        int visibleItemCount = mLayoutManager.getChildCount();
                        int totalItemCount = mLayoutManager.getItemCount();
                        int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                        if (loading)
                        {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;

                            }
                        }
                        if (!loading && (totalItemCount - visibleItemCount) <= (pastVisibleItems + visibleThreshold)) {
                            loading = true;
                            getMatchHistory(totalItemCount, totalItemCount + 20);
                        }
                    }
                }
            });
            getMatchHistory(0, 20);
        }
        setupViewSummonerDetails();
    }

    private void getMatchHistory(int beginIndex, int endIndex) {
        String url = "https://eun1.api.riotgames.com";
        String requestParams = "&beginIndex=" + beginIndex + "&endIndex=" + endIndex;

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, url + "/lol/match/v3/matchlists/by-account/" + summoner.getAccountId() + "?api_key=" + apiKey + requestParams,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        boolean firstCase = true;
                        Log.d("Volley", "Response is: " + response);
                        Gson gson = new Gson();
                        Type listType = new TypeToken<MatchListModel>() {}.getType();
                        MatchListModel matchList = gson.fromJson(response, listType);

                        if (mListItems.size() > 0) {
                            firstCase = false;
                        }
                        for (Match match: matchList.getMatches()) {
                            mListItems.add(match);
                            if (!firstCase) {
                                matchHistoryAdapter.notifyItemInserted(mListItems.size());
                            }
                        }
                        if (firstCase) {
                            mRecyclerView.setAdapter(matchHistoryAdapter);
                        }
                        summonerSearched = getResources().getString(R.string.summoner_searched);
                        SharedPreferences state = getSharedPreferences(summonerSearched, MODE_PRIVATE);
                        SharedPreferences.Editor editor = state.edit();
                        editor.putString("summonerMatchHistory", gson.toJson(matchList));
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Volley", error.getMessage());
                    }
                });
        RequestQueue queue = MMApplication.getQue();
        queue.add(stringRequest);
    }

    private void setupViewSummonerDetails() {
        LinearLayout container = findViewById(R.id.summoner_details_container);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://eun1.api.riotgames.com";
                StringRequest stringRequest = new StringRequest(
                        Request.Method.GET, url + "/lol/champion-mastery/v3/champion-masteries/by-summoner/" + summoner.getId() + "?api_key=" + apiKey,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("Volley", "Response is: " + response);
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<ChampionMastery>>() {}.getType();
                                List<ChampionMastery> championMasteryList = gson.fromJson(response, listType);
                                Intent intent = new Intent(getApplicationContext(), SummonerDetailActivity.class);
                                intent.putExtra("ChampionMasteryList", gson.toJson(championMasteryList));
                                intent.putExtra("SummonerModel", summoner);
                                startActivity(intent);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Volley", error.getMessage());
                            }
                        });
                RequestQueue queue = MMApplication.getQue();
                queue.add(stringRequest);
            }
        });
    }
}
