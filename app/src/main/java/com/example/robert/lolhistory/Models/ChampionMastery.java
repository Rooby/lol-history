package com.example.robert.lolhistory.Models;

public class ChampionMastery {
    private int championLevel;
    private long championPoints;
    private int championId;

    public int getChampionLevel() {
        return championLevel;
    }

    public long getChampionPoints() {
        return championPoints;
    }

    public int getChampionId() {
        return championId;
    }
}
