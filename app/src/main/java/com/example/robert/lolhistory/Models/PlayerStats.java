package com.example.robert.lolhistory.Models;

public class PlayerStats {
    private int neutralMinionsKilledTeamJungle;
    private int visionScore;
    private int magicDamageDealtToChampions;
    private int largestMultiKill;
    private int kills;
    private int neutralMinionsKilled;
    private long physicalDamageDealtToChampions;
    private long totalDamageTaken;
    private int largestKillingSpree;
    private long magicDamageDealt;
    public int item0;
    public int item1;
    public int item2;
    public int item3;
    public int item4;
    public int item5;
    public int item6;
    private long damageSelfMitigated;
    private long magicalDamageTaken;
    private int assists;
    private long physicalDamageDealt;
    private long goldEarned;
    private int sightWardsBoughtInGame;
    private long totalDamageDealtToChampions;
    private long physicalDamageTaken;
    private long totalDamageDealt;
    private int neutralMinionsKilledEnemyJungle;
    private int deaths;
    private int wardsPlaced;
    private int champLevel;
    private long totalHeal;
    private int totalMinionsKilled;

    public int getNeutralMinionsKilledTeamJungle() {
        return neutralMinionsKilledTeamJungle;
    }

    public int getVisionScore() {
        return visionScore;
    }

    public int getMagicDamageDealtToChampions() {
        return magicDamageDealtToChampions;
    }

    public int getLargestMultiKill() {
        return largestMultiKill;
    }

    public int getKills() {
        return kills;
    }

    public int getNeutralMinionsKilled() {
        return neutralMinionsKilled;
    }

    public long getPhysicalDamageDealtToChampions() {
        return physicalDamageDealtToChampions;
    }

    public long getTotalDamageTaken() {
        return totalDamageTaken;
    }

    public int getLargestKillingSpree() {
        return largestKillingSpree;
    }

    public long getMagicDamageDealt() {
        return magicDamageDealt;
    }

    public int getItem0() { return item0; }

    public int getItem1() {
        return item1;
    }

    public int getItem2() {
        return item2;
    }

    public int getItem3() {
        return item3;
    }

    public int getItem4() {
        return item4;
    }

    public int getItem5() {
        return item5;
    }

    public int getItem6() {
        return item6;
    }

    public long getDamageSelfMitigated() {
        return damageSelfMitigated;
    }

    public long getMagicalDamageTaken() {
        return magicalDamageTaken;
    }

    public int getAssists() {
        return assists;
    }

    public long getPhysicalDamageDealt() {
        return physicalDamageDealt;
    }

    public long getGoldEarned() {
        return goldEarned;
    }

    public int getSightWardsBoughtInGame() {
        return sightWardsBoughtInGame;
    }

    public long getTotalDamageDealtToChampions() {
        return totalDamageDealtToChampions;
    }

    public long getPhysicalDamageTaken() {
        return physicalDamageTaken;
    }

    public long getTotalDamageDealt() {
        return totalDamageDealt;
    }

    public int getNeutralMinionsKilledEnemyJungle() {
        return neutralMinionsKilledEnemyJungle;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getWardsPlaced() {
        return wardsPlaced;
    }

    public int getChampLevel() {
        return champLevel;
    }

    public long getTotalHeal() {
        return totalHeal;
    }

    public int getTotalMinionsKilled() {
        return totalMinionsKilled;
    }
}
