package com.example.robert.lolhistory.Models;

public class Participant {
    private int championId;
    private int spell1Id;
    private int spell2Id;
    private String highestAchievedSeasonTier;
    private PlayerStats stats;

    private int teamId;

    public int getParticipantId() {
        return participantId;
    }

    private int participantId;

    public int getChampionId() {
        return championId;
    }

    public int getSpell1Id() {
        return spell1Id;
    }

    public int getSpell2Id() {
        return spell2Id;
    }

    public String getHighestAchievedSeasonTier() {
        return highestAchievedSeasonTier;
    }

    public PlayerStats getStats() {
        return stats;
    }

    public int getTeamId() {
        return teamId;
    }
}
