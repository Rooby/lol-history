package com.example.robert.lolhistory.Models;

public class Match {
    private String lane;
    private long gameId;
    private int champion;
    private String platformId;
    private long timestamp;
    private int queue;
    private String role;
    private int season;

    public String getLane() {
        return lane;
    }

    public long getGameId() {
        return gameId;
    }

    public int getChampion() {
        return champion;
    }

    public String getPlatformId() {
        return platformId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getQueue() {
        return queue;
    }

    public String getRole() {
        return role;
    }

    public int getSeason() {
        return season;
    }
}
