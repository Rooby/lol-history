package com.example.robert.lolhistory.Models;

import java.util.List;

public class GameModel {
    private int seasonId;
    private String gameMode;
    private List<ParticipantIdentity> participantIdentities;
    private List<Team> teams;
    private List<Participant> participants;

    public int getSeasonId() {
        return seasonId;
    }

    public String getGameMode() {
        return gameMode;
    }

    public List<ParticipantIdentity> getParticipantIdentities() {
        return participantIdentities;
    }

    public List<Team> getStats() {
        return teams;
    }

    public int getGameDuration() {
        return gameDuration;
    }

    private int gameDuration;

    public List<Team> getTeams() {
        return teams;
    }

    public List<Participant> getParticipants() {
        return participants;
    }
}
