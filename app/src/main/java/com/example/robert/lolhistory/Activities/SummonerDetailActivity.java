package com.example.robert.lolhistory.Activities;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.robert.lolhistory.Helpers.DownloadImageTask;
import com.example.robert.lolhistory.Models.ChampionList;
import com.example.robert.lolhistory.Models.ChampionMastery;
import com.example.robert.lolhistory.Models.ChampionMasteryList;
import com.example.robert.lolhistory.Models.ChampionModel;
import com.example.robert.lolhistory.Models.SummonerModel;
import com.example.robert.lolhistory.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.List;

public class SummonerDetailActivity extends AppCompatActivity {

    private List<ChampionMastery> championMasteryList;
    private SummonerModel summonerModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summoner_detail);

        String strObj = getIntent().getStringExtra("ChampionMasteryList");
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ChampionMastery>>() {}.getType();
        List<ChampionMastery> championMasteryList = gson.fromJson(strObj, listType);

        summonerModel = (SummonerModel) getIntent().getSerializableExtra("SummonerModel");

        TextView summonerName = findViewById(R.id.summoner_name);
        summonerName.setText(summonerModel.getName());
        TextView summonerLevel = findViewById(R.id.summoner_level);
        summonerLevel.setText("(" + summonerModel.getSummonerLevel() + ")");

        String imagePath = "http://ddragon.leagueoflegends.com/cdn/8.11.1/img/profileicon/";
        ImageView summonerIcon = findViewById(R.id.summoner_icon);
        new DownloadImageTask(summonerIcon).execute(imagePath + summonerModel.getProfileIconId() + ".png");

        TextView favorite1Level = findViewById(R.id.favorite_1_level);
        favorite1Level.setText("(" + championMasteryList.get(0).getChampionLevel() + ")");
        ImageView favorite1Icon = findViewById(R.id.favorite_1);
        TextView favorite1Name = findViewById(R.id.favorite_1_name);
        getChampionInfo(championMasteryList.get(0).getChampionId(), favorite1Name, favorite1Icon);

        TextView favorite2Level = findViewById(R.id.favorite_2_level);
        favorite2Level.setText("(" + championMasteryList.get(1).getChampionLevel() + ")");
        ImageView favorite2Icon = findViewById(R.id.favorite_2);
        TextView favorite2Name = findViewById(R.id.favorite_2_name);
        getChampionInfo(championMasteryList.get(1).getChampionId(), favorite2Name, favorite2Icon);

        TextView favorite3Level = findViewById(R.id.favorite_3_level);
        favorite3Level.setText("(" + championMasteryList.get(2).getChampionLevel() + ")");
        ImageView favorite3Icon = findViewById(R.id.favorite_3);
        TextView favorite3Name = findViewById(R.id.favorite_3_name);
        getChampionInfo(championMasteryList.get(2).getChampionId(), favorite3Name, favorite3Icon);

    }

    private void getChampionInfo(int championId, final TextView championText, final ImageView championImage) {
        final String imagePath = "https://ddragon.leagueoflegends.com/cdn/7.10.1/img/champion/";

        InputStream is = getResources().openRawResource(R.raw.champion_data);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (Exception e) {
            Log.d("JSON Error", e.getMessage());
        } finally {
            try {
                is.close();
            } catch (Exception e) {
                Log.d("JSON Error", e.getMessage());
            }
        }

        String jsonString = writer.toString();
        Gson gson = new Gson();

        Type listType = new TypeToken<ChampionList>() {}.getType();
        ChampionList championsList = gson.fromJson(jsonString, listType);

        for (ChampionModel champion: championsList.getData()) {
            if (champion.getId() == championId) {
                championText.setText(champion.getName());
                new DownloadImageTask(championImage).execute(imagePath + champion.getKey() + ".png");
                break;
            }
        }
    }
}
