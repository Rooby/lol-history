package com.example.robert.lolhistory.Models;

public class ChampionModel {
    private String title;
    private String name;
    private int id;
    private String key;

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
