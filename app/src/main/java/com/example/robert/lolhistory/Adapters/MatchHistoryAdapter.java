package com.example.robert.lolhistory.Adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.robert.lolhistory.Activities.MatchDetailActivity;
import com.example.robert.lolhistory.Helpers.DownloadImageTask;
import com.example.robert.lolhistory.Models.ChampionList;
import com.example.robert.lolhistory.Models.ChampionModel;
import com.example.robert.lolhistory.Models.Match;
import com.example.robert.lolhistory.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.List;

public class MatchHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> mItemsList;
    private Context mContext;

    public MatchHistoryAdapter(List<Object> list, Context context){
        mItemsList = list;
        mContext = context;
    }
    @Override
    public int getItemCount() {
        return mItemsList.size();
    }
    @Override
    public long getItemId(int i) {
        //Not needed
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View view = View.inflate(mContext, R.layout.row_match_min_data, null);
        holder = new MatchMinDataViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ((MatchMinDataViewHolder) holder).bindView((Match) mItemsList.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, MatchDetailActivity.class);
                intent.putExtra("gameId", ((Match) mItemsList.get(position)).getGameId());
                mContext.startActivity(intent);
            } });
    }

    private class MatchMinDataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView role;
        public TextView champion;
        public TextView matchDate;
        public ImageView championImage;

        public MatchMinDataViewHolder(View itemView) {
            super(itemView);
            this.role = (TextView) itemView.findViewById(R.id.role_data);
            this.champion = (TextView) itemView.findViewById(R.id.champion_data);
            this.matchDate = (TextView) itemView.findViewById(R.id.match_date_data);
            this.championImage = (ImageView) itemView.findViewById(R.id.champion_image);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void bindView(Match match) {
            String role = match.getLane();
            if (role == "NONE") {
                role = "SUPPORT";
            }
            this.role.setText(role);
            getChampionInfo(match.getChampion(), this.champion, this.championImage);
            this.matchDate.setText(getDate(match.getTimestamp()));
        }

        @Override
        public void onClick(View view) {
        }

        @Override
        public boolean onLongClick(View view) {
            return true;
        }

        private String getDate(long timeStamp) {

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                Date netDate = (new Date(timeStamp));
                return sdf.format(netDate);
            } catch (Exception ex) {
                return "xx";
            }
        }

        private void getChampionInfo(int championId, final TextView championText, final ImageView championImage) {
            final String imagePath = "https://ddragon.leagueoflegends.com/cdn/7.10.1/img/champion/";

            InputStream is = mContext.getResources().openRawResource(R.raw.champion_data);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (Exception e) {
                Log.d("JSON Error", e.getMessage());
            } finally {
                try {
                    is.close();
                } catch (Exception e) {
                    Log.d("JSON Error", e.getMessage());
                }
            }

            String jsonString = writer.toString();
            Gson gson = new Gson();

            Type listType = new TypeToken<ChampionList>() {}.getType();
            ChampionList championsList = gson.fromJson(jsonString, listType);
            Log.d("JSON Error", championsList.toString());
            for (ChampionModel champion: championsList.getData()) {
                if (champion.getId() == championId) {
                    championText.setText(champion.getName() + " " + champion.getTitle());
                    new DownloadImageTask(championImage).execute(imagePath + champion.getKey() + ".png");
                    break;
                }
            }
        }
    }
}
