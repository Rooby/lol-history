package com.example.robert.lolhistory.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.robert.lolhistory.Helpers.DownloadImageTask;
import com.example.robert.lolhistory.Models.ChampionList;
import com.example.robert.lolhistory.Models.ChampionModel;
import com.example.robert.lolhistory.Models.Participant;
import com.example.robert.lolhistory.Models.ParticipantIdentity;
import com.example.robert.lolhistory.Models.Player;
import com.example.robert.lolhistory.Models.PlayerStats;
import com.example.robert.lolhistory.Models.Team;
import com.example.robert.lolhistory.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.List;

public class MatchDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ParticipantIdentity> participantIdentitiesList;
    private List<Participant> participantList;
    private List<Team> teamList;
    private Context mContext;

    public MatchDetailsAdapter(List<ParticipantIdentity> participantIdentitiesList, List<Participant> participantLis, List<Team> teamList, Context context){
        this.participantIdentitiesList = participantIdentitiesList;
        this.participantList = participantLis;
        this.teamList = teamList;
        mContext = context;
    }
    @Override
    public int getItemCount() {
        return teamList.size();
    }
    @Override
    public long getItemId(int i) {
        //Not needed
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View view = View.inflate(mContext, R.layout.row_player_match_data, null);
        holder = new MatchDetailsAdapter.MatchMaxDataViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ((MatchDetailsAdapter.MatchMaxDataViewHolder) holder).bindView( this.teamList.get(position), position);
    }

    private class MatchMaxDataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView teamName;
        public TextView teamKda;
        public TextView teamTotalGold;
        public TextView teamTowers;
        public TextView teamInhibitors;
        public TextView teamDragons;
        public TextView teamRiftHeralds;
        public TextView teamBarons;
        public TextView teamStatus;

        public TextView championLevel1;
        public TextView championLevel2;
        public TextView championLevel3;
        public TextView championLevel4;
        public TextView championLevel5;

        public ImageView championIcon1;
        public ImageView championIcon2;
        public ImageView championIcon3;
        public ImageView championIcon4;
        public ImageView championIcon5;


        public TextView summonerName1;
        public TextView summonerName2;
        public TextView summonerName3;
        public TextView summonerName4;
        public TextView summonerName5;

        public ImageView item0_1;
        public ImageView item1_1;
        public ImageView item2_1;
        public ImageView item3_1;
        public ImageView item4_1;
        public ImageView item5_1;
        public ImageView item6_1;

        public ImageView item0_2;
        public ImageView item1_2;
        public ImageView item2_2;
        public ImageView item3_2;
        public ImageView item4_2;
        public ImageView item5_2;
        public ImageView item6_2;

        public ImageView item0_3;
        public ImageView item1_3;
        public ImageView item2_3;
        public ImageView item3_3;
        public ImageView item4_3;
        public ImageView item5_3;
        public ImageView item6_3;

        public ImageView item0_4;
        public ImageView item1_4;
        public ImageView item2_4;
        public ImageView item3_4;
        public ImageView item4_4;
        public ImageView item5_4;
        public ImageView item6_4;

        public ImageView item0_5;
        public ImageView item1_5;
        public ImageView item2_5;
        public ImageView item3_5;
        public ImageView item4_5;
        public ImageView item5_5;
        public ImageView item6_5;

        public TextView kda1;
        public TextView kda2;
        public TextView kda3;
        public TextView kda4;
        public TextView kda5;

        public TextView minionKills1;
        public TextView minionKills2;
        public TextView minionKills3;
        public TextView minionKills4;
        public TextView minionKills5;

        public TextView goldScore1;
        public TextView goldScore2;
        public TextView goldScore3;
        public TextView goldScore4;
        public TextView goldScore5;

        public MatchMaxDataViewHolder(View itemView) {
            super(itemView);
            this.teamName = itemView.findViewById(R.id.teamName);
            this.teamKda = itemView.findViewById(R.id.total_team_kda);
            this.teamTotalGold = itemView.findViewById(R.id.total_team_gold);
            this.teamTowers = itemView.findViewById(R.id.towers);
            this.teamInhibitors = itemView.findViewById(R.id.inhibitors);
            this.teamDragons = itemView.findViewById(R.id.dragons);
            this.teamRiftHeralds = itemView.findViewById(R.id.rift_heralds);
            this.teamBarons = itemView.findViewById(R.id.barons);
            this.teamStatus = itemView.findViewById(R.id.team_status);

            this.initChampionLevelElements(itemView);
            this.initChampionIconElements(itemView);
            this.initSummonerNameElements(itemView);
            this.initItemElements(itemView);
            this.initKdaElements(itemView);
            this.initMinionKillsElements(itemView);
            this.initGoldScoreElements(itemView);
        }

        public void bindView(Team team, int teamPosition) {
            int size = participantList.size();
            int i, counter = 1;
            String win = team.getWin();

            if (win.equals("Fail")) {
                win = "Lose";
            }

            int teamTotalKills = 0;
            int teamTotalDeaths = 0;
            int teamTotalAssists = 0;
            int teamTotalGold = 0;

            if (teamPosition == 0) {
                i = 0;
                size -= size / 2;
            } else {
                i = 5;
            }

            this.teamName.setText("Team " + (teamPosition + 1));
            this.teamStatus.setText("" + win);

            for (; i < size; i++) {
                ParticipantIdentity participantIdentity = participantIdentitiesList.get(i);
                Participant participant = participantList.get(i);
                Player player = participantIdentity.getPlayer();
                PlayerStats stats = participant.getStats();

                try {
                    int championId = participant.getChampionId();
                    ImageView championIcon = (ImageView) MatchMaxDataViewHolder.class.getField("championIcon" + counter).get(this);
                    getChampionInfo(championId, championIcon);

                    TextView summonerName = (TextView) MatchMaxDataViewHolder.class.getField("summonerName" + counter).get(this);
                    summonerName.setText("" + player.getSummonerName());
                    TextView championLevel = (TextView) MatchMaxDataViewHolder.class.getField("championLevel" + counter).get(this);
                    championLevel.setText("" + stats.getChampLevel());


                    for (int j = 0; j < 7; j++) {
                        ImageView item = (ImageView) MatchMaxDataViewHolder.class.getField("item" + j + "_" + counter).get(this);
                        try {
                            int itemId = (int ) stats.getClass().getField("item" + j).get(stats);
                            getItemImage(itemId, item);
                        } catch (Exception e) {
                            Log.d("Dynamic method", e.getMessage());
                        }
                    }

                    TextView kda = (TextView) MatchMaxDataViewHolder.class.getField("kda" + counter).get(this);
                    kda.setText("" + stats.getKills() + "/" + stats.getDeaths() + "/" + stats.getAssists());
                    teamTotalKills += stats.getKills();
                    teamTotalDeaths += stats.getDeaths();
                    teamTotalAssists += stats.getAssists();

                    TextView minionScore = (TextView) MatchMaxDataViewHolder.class.getField("minionKills" + counter).get(this);
                    minionScore.setText("" + stats.getTotalMinionsKilled());

                    TextView goldScore = (TextView) MatchMaxDataViewHolder.class.getField("goldScore" + counter).get(this);
                    goldScore.setText("" + stats.getGoldEarned());
                    teamTotalGold += stats.getGoldEarned();

                    counter++;
                } catch (Exception e) {
                    Log.d("Dynamic fields", e.getMessage());
                }

            }
            this.teamKda.setText(teamTotalKills + "/" + teamTotalDeaths + "/" + teamTotalAssists);
            this.teamTotalGold.setText("" + teamTotalGold);
            this.teamTowers.setText("" + team.getTowerKills());
            this.teamInhibitors.setText("" + team.getInhibitorKills());
            this.teamDragons.setText("" + team.getDragonKills());
            this.teamRiftHeralds.setText("" + team.getRiftHeraldKills());
            this.teamBarons.setText("" + team.getBaronKills());
        }

        @Override
        public void onClick(View view) {
        }

        @Override
        public boolean onLongClick(View view) {
            return true;
        }

        private void getItemImage(int itemId, final ImageView itemImage) {
            final String imagePath = "https://ddragon.leagueoflegends.com/cdn/7.10.1/img/item/";
            new DownloadImageTask(itemImage).execute(imagePath + itemId + ".png");
        }

        private void getChampionInfo(int championId, final ImageView championImage) {
            final String imagePath = "https://ddragon.leagueoflegends.com/cdn/7.10.1/img/champion/";

            InputStream is = mContext.getResources().openRawResource(R.raw.champion_data);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } catch (Exception e) {
                Log.d("JSON Error", e.getMessage());
            } finally {
                try {
                    is.close();
                } catch (Exception e) {
                    Log.d("JSON Error", e.getMessage());
                }
            }

            String jsonString = writer.toString();
            Gson gson = new Gson();

            Type listType = new TypeToken<ChampionList>() {}.getType();
            ChampionList championsList = gson.fromJson(jsonString, listType);
            for (ChampionModel champion: championsList.getData()) {
                if (champion.getId() == championId) {
                    new DownloadImageTask(championImage).execute(imagePath + champion.getKey() + ".png");
                    break;
                }
            }
        }

        private void initItemElements(View itemview) {
            this.item0_1 = itemview.findViewById(R.id.item0);
            this.item1_1 = itemview.findViewById(R.id.item1);
            this.item2_1 = itemview.findViewById(R.id.item2);
            this.item3_1 = itemview.findViewById(R.id.item3);
            this.item4_1 = itemview.findViewById(R.id.item4);
            this.item5_1 = itemview.findViewById(R.id.item5);
            this.item6_1 = itemview.findViewById(R.id.item6);

            this.item0_2 = itemview.findViewById(R.id.item0_2);
            this.item1_2 = itemview.findViewById(R.id.item1_2);
            this.item2_2 = itemview.findViewById(R.id.item2_2);
            this.item3_2 = itemview.findViewById(R.id.item3_2);
            this.item4_2 = itemview.findViewById(R.id.item4_2);
            this.item5_2 = itemview.findViewById(R.id.item5_2);
            this.item6_2 = itemview.findViewById(R.id.item6_2);

            this.item0_3 = itemview.findViewById(R.id.item0_3);
            this.item1_3 = itemview.findViewById(R.id.item1_3);
            this.item2_3 = itemview.findViewById(R.id.item2_3);
            this.item3_3 = itemview.findViewById(R.id.item3_3);
            this.item4_3 = itemview.findViewById(R.id.item4_3);
            this.item5_3 = itemview.findViewById(R.id.item5_3);
            this.item6_3 = itemview.findViewById(R.id.item6_3);

            this.item0_4 = itemview.findViewById(R.id.item0_4);
            this.item1_4 = itemview.findViewById(R.id.item1_4);
            this.item2_4 = itemview.findViewById(R.id.item2_4);
            this.item3_4 = itemview.findViewById(R.id.item3_4);
            this.item4_4 = itemview.findViewById(R.id.item4_4);
            this.item5_4 = itemview.findViewById(R.id.item5_4);
            this.item6_4 = itemview.findViewById(R.id.item6_4);

            this.item0_5 = itemview.findViewById(R.id.item0_5);
            this.item1_5 = itemview.findViewById(R.id.item1_5);
            this.item2_5 = itemview.findViewById(R.id.item2_5);
            this.item3_5 = itemview.findViewById(R.id.item3_5);
            this.item4_5 = itemview.findViewById(R.id.item4_5);
            this.item5_5 = itemview.findViewById(R.id.item5_5);
            this.item6_5 = itemview.findViewById(R.id.item6_5);
        }

        private void initKdaElements(View itemview) {
            this.kda1 = itemview.findViewById(R.id.kda);
            this.kda2 = itemview.findViewById(R.id.kda2);
            this.kda3 = itemview.findViewById(R.id.kda3);
            this.kda4 = itemview.findViewById(R.id.kda4);
            this.kda5 = itemview.findViewById(R.id.kda5);
        }

        private void initMinionKillsElements(View itemview) {
            this.minionKills1 = itemview.findViewById(R.id.minion_kills);
            this.minionKills2 = itemview.findViewById(R.id.minion_kills2);
            this.minionKills3 = itemview.findViewById(R.id.minion_kills3);
            this.minionKills4 = itemview.findViewById(R.id.minion_kills4);
            this.minionKills5 = itemview.findViewById(R.id.minion_kills5);
        }

        private void initGoldScoreElements(View itemview) {
            this.goldScore1 = itemview.findViewById(R.id.gold_score);
            this.goldScore2 = itemview.findViewById(R.id.gold_score2);
            this.goldScore3 = itemview.findViewById(R.id.gold_score3);
            this.goldScore4 = itemview.findViewById(R.id.gold_score4);
            this.goldScore5 = itemview.findViewById(R.id.gold_score5);
        }

        private void initChampionLevelElements(View itemview) {
            this.championLevel1 = itemview.findViewById(R.id.champion_level);
            this.championLevel2 = itemview.findViewById(R.id.champion_level2);
            this.championLevel3 = itemview.findViewById(R.id.champion_level3);
            this.championLevel4 = itemview.findViewById(R.id.champion_level4);
            this.championLevel5 = itemview.findViewById(R.id.champion_level5);
        }

        private void initChampionIconElements(View itemview) {
            this.championIcon1 = itemview.findViewById(R.id.champion_icon);
            this.championIcon2= itemview.findViewById(R.id.champion_icon2);
            this.championIcon3= itemview.findViewById(R.id.champion_icon3);
            this.championIcon4= itemview.findViewById(R.id.champion_icon4);
            this.championIcon5= itemview.findViewById(R.id.champion_icon5);
        }

        private void initSummonerNameElements(View itemview) {
            this.summonerName1 = itemview.findViewById(R.id.summoner_name);
            this.summonerName2 = itemview.findViewById(R.id.summoner_name2);
            this.summonerName3 = itemview.findViewById(R.id.summoner_name3);
            this.summonerName4 = itemview.findViewById(R.id.summoner_name4);
            this.summonerName5 = itemview.findViewById(R.id.summoner_name5);
        }
    }
}
